package com.battcn.platform.service;

import com.battcn.framework.mybatis.page.DataGrid;
import com.battcn.framework.mybatis.service.BaseService;
import com.battcn.platform.pojo.po.Log;
import com.github.pagehelper.PageInfo;

/**
 * @author Levin
 */
public interface LogService extends BaseService<Log> {

    /**
     * 分页查询日志信息
     *
     * @param grid     分页对象
     * @param datetime 查询日期
     * @return 分页结果
     */
    PageInfo<Log> listForDataGrid(DataGrid grid, String datetime);

}
